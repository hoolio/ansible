#!/bin/false
#
# this content was produced for usa at OSDC 2015 in Hobart.
#  please use and reuse subject to CC by Julius Roberts

### please ask questions anytime

### local installation
sudo apt-get install ansible git
ansible --version
# ansible 1.5.4
git clone https://gitlab.com/hoolio/ansible.git
cd ansible
git checkout osdc
cp hosts.example /etc/ansible/hosts
vi /etc/ansible/hosts

### remote bootstrapping
#  ssh-copy-id remotehost
#  ssh-copy-id root@remotehost
#  ansible all -m raw -a 'sudo apt-get install -y python-simplejson'

### command module
cat /etc/ansible/hosts
ansible push_mode_hosts -o -m command -a 'date'
ansible push_mode_hosts -o -a 'date'
ansible all -o -a 'whoami'
ansible all -f 20 -o -a 'whoami'
ansible all -f 20 -s -o -a 'whoami'

grep forks /etc/ansible/ansible.cfg
sudo vi /etc/ansible/ansible.cfg

### other modules and ansible-doc
ansible all -m ping
ansible-doc -l
ansible-doc -l | wc -l
ansible-doc apt -s
ansible push_mode_hosts -o -s -m apt -a 'pkg=ntp state=absent' --limit ansible2
ansible push_mode_hosts -o -s -m apt -a 'pkg=ntp state=installed update_cache=yes cache_valid_time=3600'

### ansible_playbook and tasks
cat 1_ping.yml
ansible-playbook 1_ping.yml
cat 2_ntp.yml
cat tasks/2_ntp.yml

### facts and templates
cat templates/etc-ntp.conf.j2 | grep "{{"
ansible all -m setup --tree /tmp/facts
grep -Ri ansible_hostname /tmp/facts/

### ansible playbooks continued
cat 2_ntp.yml
ansible-playbook 2_ntp.yml
ansible push_mode_hosts -o -a 'grep xxxxxx /etc/ntp.conf'

### ansible-pull on single host
cat local.yml
ansible ansible6 -s -m apt -a 'pkg=ansible state=installed'
ssh ansible6 ansible-pull -d /usr/local/ansible/ -U https://github.com/hooliowobbits/ansible.git -C osdc
ssh ansible6 grep yyyyyyy /etc/ntp.conf

# ansible-pull on multiple hosts via ansible and cron
less 3_ansible_pull.yml
ansible pull_mode_hosts -o -a 'cat /etc/cron.d/ansible-pull'
...
ansible pull_mode_hosts -o -a 'grep yyyyyy /etc/ntp.conf'

# grabbing logs for ansible-pull
ansible pull_mode_hosts -o -m fetch -a 'src=/var/log/ansible-pull.log dest=./fetched'
tree ./fetched/
grep -R ERROR ./fetched/

### ansible galaxy and roles
sudo ansible-galaxy install bennojoy.ntp
ansible-galaxy list
cat 4_ntp.yml
less /etc/ansible/roles/bennojoy.ntp/tasks/main.yml
ansible-playbook 4_ntp.yml
ansible role_mode_hosts -o -a 'grep zzzzz /etc/ntp.conf'

### conclusion
ansible all -o -s -a 'grep ^server /etc/ntp.conf' | grep -v debian | sort

### questions?
